import React from 'react'
import * as BooksAPI from  './BooksAPI.js'
import {withRouter} from 'react-router-dom'
//import {Router} from 'react-router'
//import PropType from 'prop-types'

class Search extends React.Component{

	constructor(props){ console.log(props)
		super(props)
	this.state ={
		query:'',
		searchresult:[]
	}

	}

	
	
upadateQuery =(query)=>{ 
	this.setState({query:query},()=>{

		BooksAPI.search(this.state.query).then((book)=>{ //console.log('book',book)
		
    		this.setState({searchresult:book})
    		
		
	})
	})
	
	

  }
 backNav =(e)=>{
 	e.preventDefault()
 	//console.log(this.props)
this.props.history.goBack();
 }


	render(){
		let showBOOKS
		if(this.state.query){
			
			showBOOKS=this.state.searchresult
			
		}else{
			showBOOKS=this.props.books
		}
		//showcontact=showcontact.sort(Sortby('name'))
		return(
			<div className="search-books">
            <div className="search-books-bar">
              <a className="close-search" onClick={(event)=>{this.backNav(event)}}>Close</a>
              <div className="search-books-input-wrapper">
                {}
                <input type="text" placeholder="Search by title or author" value={this.state.query} onChange={(event)=>this.upadateQuery(event.target.value)}/>

              </div>
            </div>
            <div className="search-books-results">
              <ol className="books-grid">
              {(showBOOKS&&showBOOKS.length)?(
              showBOOKS.map((data1,index)=>(

              	<li key={index}>
                      
                        <div className="book">
                          <div className="book-top">
                            <div className="book-cover" style={{ width: 128, height: 192, backgroundImage: `url(${data1.imageLinks.thumbnail})` }}></div>
                            <div className="book-shelf-changer">
                              <select id="dropdown" onChange={(event)=>this.props.updateshelf(data1,event.target.value)}>
                                <option value="move" disabled>Move to...</option>
                                <option value="currentlyReading">Currently Reading</option>
                                <option value="wantToRead">Want to Read</option>
                                <option value="read">Read</option>
                                <option value="none">None</option>
                              </select>
                            </div>
                          </div>
                          <div className="book-title">{data1.title}</div>
                          <div className="book-authors">{data1.authors&&data1.authors[0]?data1.authors[0]:null}</div>
                        </div>
                      </li>

              	))):(<div>Data Not Found</div>)
				}
              </ol>
            </div>
          </div>

			)
	}
}
const searchwithroute = withRouter(Search)

export default searchwithroute
