import React from 'react'
import {Route,link} from 'react-router-dom'
import Search from './search.js'
import Currentread from  './currentread.js'
import Wantread from  './wantread.js'
import Read from  './read.js'
import * as BooksAPI from  './BooksAPI.js'
// import * as BooksAPI from './BooksAPI'
import './App.css'

class BooksApp extends React.Component {
  state = {
    /**
     * TODO: Instead of using this state variable to keep track of which page
     * we're on, use the URL in the browser's address bar. This will ensure that
     * users can use the browser's back and forward buttons to navigate between
     * pages, as well as provide a good URL they can bookmark and share.
     */
    showSearchPage: false,
    books:[]
  }
statechnge  = () => {

  this.setState({showSearchPage:false})


}
updateshelf(data,shelf){ 
  
  
BooksAPI.update(data,shelf).then((response)=>{ 
    
  })

}
componentDidMount(){ //console.log(this.props);

  BooksAPI.getAll().then((book)=>{ //console.log(book);
    this.setState({books:book})
  })


}

  render() {
    //console.log(this.props);
    return (
      <div className="app">
      
        <Route exact path="/search" render ={(props)=>( 
          <Search back ={this.statechnge} books={this.state.books} updateshelf={(data,shelf)=>this.updateshelf(data,shelf)} />
          
        ) } history={this.props.history}/> 


        <Route exact path="/" render={(props)=>(
          <div className="list-books">
            <div className="list-books-title">
              <h1>MyReads</h1>
            </div>
            <div className="list-books-content">
              <div>
                <Currentread books={this.state.books} />
                <Wantread books={this.state.books} />
                <Read books={this.state.books} />
                
                
              </div>
            </div>
            <div className="open-search">
              <a href="/search">Add a book</a>
            </div>
          </div>
        )} />

      </div>
    )
  }
}

export default BooksApp
